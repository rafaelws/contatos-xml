package contatos;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class ContatosCLI {

  private ContatosService service;
	private Scanner s;

	public ContatosCLI() throws ParserConfigurationException, SAXException, IOException {
		service = new ContatosXML();
	}

	public void init(Scanner s) {
		this.s = s;

		Integer opt;
		String beforeOpt;
		Boolean isExecuting = true;

		while (isExecuting) {
			System.out.println("\n Selecione uma opção: \n 1 - Listar \n 2 - Buscar \n 3 - Incluir \n 4 - Alterar \n 5 - Remover \n 6 - Sair");
			beforeOpt = s.nextLine();

			if (isNumber(beforeOpt)) {
				opt = Integer.parseInt(beforeOpt);

				switch (opt) {

					// Listar
					case 1 : listar(); break;

					// Buscar
					case 2 : buscar(); break;

					// Incluir
					case 3 : incluir(); break;

					// Alterar
					case 4 : alterar(); break;

					// Remover
					case 5 : { 
						Contato c = buscar();
						if (null != c) {
							remover(c);
						}
						break;
					}

					// SAIR
					case 6 : { 
						isExecuting = false;
						System.out.println("Encerrando!");
						System.exit(0);
						break;
					}

					// invalido
					default : { 
						System.out.println(">> Valor invalido <<");
						break; 
					}

				}
			} else {
				System.out.println("Opção inválida!");
			}
		}
	}
	
	public void listar() {
		List<Contato> contatos = service.listar();
		if (contatos.isEmpty()) {
			System.out.println("No momento, não existem contatos para serem mostrados.");
		} else {
			System.out.println(">> Contatos: ");
			for (Contato c : contatos) {
				System.out.println(">> " + c.toString());
			}
		}
	}
	
	public void incluir() {
		Contato c = novoContato( null );
		service.incluir( c );
		System.out.println(">> Contato adicionado!");
	}
	
	public void alterar() {
		Contato c = buscar();
		if (null != c) {
			c = novoContato( c );
			service.remover( c.getId() );
			service.incluir(c);
			System.out.println(">> Contato alterado!");
		}
	}

	private void remover(Contato c) {
		System.out.println("Este é o contato que você deseja remover? [S/N]");

		String answer = s.nextLine();
		if (answer.equalsIgnoreCase("S")) {
			service.remover(c.getId());
			System.out.println(">> Contato removido!");
		} else if (answer.equalsIgnoreCase("N")) {
			System.out.println("O contato " + c.getNome() + " não foi removido!");
		} else {
			System.out.println("Resposta inválida. Voltando ao menu.");
		}
	}

	private Contato buscar() {
		System.out.println("Insira o código do contato: ");
		String id = s.nextLine();

		Contato c = service.buscar(id);

		if (null == c) {
			System.out.println("Não encontrado!");
		} else {
			System.out.println(">> Contato Encontrado: " + c.toString());
			return c;
		}
		return null;
	}

	private Contato novoContato(Contato cAnterior) {
		Boolean isEditando = (null != cAnterior);

		System.out.println("Insira o nome: ");
		if (isEditando) System.out.println(">> Nome atual: " + cAnterior.getNome());
		String nome = s.nextLine();

		System.out.println("Insira o Email: ");
		if (isEditando) System.out.println(">> Email atual: " + cAnterior.getEmail());
		String email = s.nextLine();

		if (isEditando) {
			System.out.println(">> Telefones Atuais: ");
			for (String s : cAnterior.getTelefones()) {
				System.out.println(" >>> " + s);
			}
		}

		System.out.println("Quantos telefones deseja inserir? ");
		String beforeTels = s.nextLine();
		String telefones[] = null;

		if (isNumber(beforeTels)) {
			Integer tels = Integer.parseInt(beforeTels);

			if (tels != 0) {
				telefones = new String[tels];

				for (int i = 0; i < tels; i++) {
					System.out.println("Insira o telefone: ");
					telefones[i] = s.nextLine();
				}
			}
		}

		return new Contato(
				(null == telefones ? null : Arrays.asList(telefones)), email, nome);
	}

	private Boolean isNumber(String s) {
		return ( null != s && !s.isEmpty() && s.matches("[0-9]+") );
	}
}