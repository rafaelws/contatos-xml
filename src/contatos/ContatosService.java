package contatos;

import java.util.List;

public interface ContatosService {

	void incluir(Contato c);
	void remover(String id);
	Contato buscar(String id);
	
	List<Contato> listar();
}
