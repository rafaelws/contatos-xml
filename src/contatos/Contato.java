package contatos;

import java.util.ArrayList;
import java.util.List;

public class Contato {

	private List<String> telefones;
	private String email;
	private String nome;
	private String id;
	
	public Contato(List<String> telefones, String email, String nome) {
		this.telefones = telefones;
		this.email = email;
		this.nome = nome;
	}
	
	public Contato(List<String> telefones, String email, String nome, String id) {
		this.telefones = telefones;
		this.email = email;
		this.nome = nome;
		this.id = id;
	}
	
	public Contato() {
	}
	
	public List<String> getTelefones() {
		return telefones;
	}
	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void addTelefone(String telefone) {
		if (telefones == null)
			telefones = new ArrayList<String>();
		telefones.add(telefone);
	}
	
	@Override
	public String toString() {
		String tels = "";
		for (String s : this.telefones) {
			tels += s + ", ";
		}
		
		return "[" + this.id + "] Nome: " + this.nome + ", Email: " + this.email + ", Telefones: " + tels;
	}
}
