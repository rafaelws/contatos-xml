package contatos;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class XMLFileHandler {

	private File f;
	private DocumentBuilder builder;
	private String rootElementName;
	
	public XMLFileHandler(String fileName, String rootElementName) throws ParserConfigurationException {
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		this.rootElementName = rootElementName;
		setup(fileName);
	}
	
	private void setup(String fileName) {
		f = new File(fileName);
		if (f.exists()) {
			System.out.println("Arquivo existe!");
		} else {
			try {
				if (f.createNewFile()) {
					setup();
					System.out.println("Criou o arquivo " + fileName + " com sucesso! Caminho: " + f.getAbsolutePath());
				} else {
					System.out.println("Aparentemente nao criou o arquivo!");
				}
			} catch (IOException e) {
				System.out.println("ERROR >> Não foi possível criar o arquivo " + fileName);
			}
		}
	}
	
	private void setup() {
		Document d = builder.newDocument();
		Element root = d.createElement(rootElementName);
		d.appendChild(root);
		
		write(d);
	}
	
	private void write( Document d ) {
		Transformer t = null;
		try {
			t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.STANDALONE, "yes");
			t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			
			t.transform(new DOMSource(d), new StreamResult(f));
		} catch (TransformerException e){
			System.out.println("ERROR >> Não foi possível escrever o documento!");
		}
	}
	
	public void addAndWrite(Document doc, Element el) {
		getRoot(doc).appendChild(el);
		write(doc);
	}
	
	public void removeAndWrite(Document doc, Element el) {
		getRoot(doc).removeChild(el);
		write(doc);
	}
	
	public Document getDocument() throws SAXException, IOException {
		return builder.parse(f);
	}
	
	public Element getRoot(Document doc) {
		return (Element) doc.getElementsByTagName(rootElementName).item(0);
	}
}
