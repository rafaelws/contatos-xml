package contatos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ContatosXML implements ContatosService {

	private Document doc;
	private XMLFileHandler fileHandler;
	private String fileName = "contatos.xml";
	private String rootElementName = "contatos";
	
	private Element foundEl;

	public ContatosXML() throws ParserConfigurationException, SAXException, IOException {
		fileHandler = new XMLFileHandler(fileName, rootElementName);
		doc = fileHandler.getDocument();
	}
	
	@Override
	public void incluir(Contato c) {		
		Element contatoEl = doc.createElement("contato");
		
		if (null == c.getId()) { // quando for incluir, preserva o ID
			c.setId(obterNovoID().toString());
		}
		
		// filhos
		Element nomeEl = doc.createElement("nome"),
				telefonesEl = doc.createElement("telefones"), // recebe +de 1 telefone
				emailEl = doc.createElement("email");
		
		// Atribui os valores
		nomeEl.appendChild(doc.createTextNode(c.getNome()));
		
		if (null != c.getTelefones()) {
			Element telAtual;
			for (String s : c.getTelefones()) {
				telAtual = doc.createElement("telefone");
				telAtual.appendChild(doc.createTextNode(s));
				telefonesEl.appendChild(telAtual);
				telAtual = null;
			}
		}
		
		emailEl.appendChild(doc.createTextNode(c.getEmail()));
		
		// atribui ao pai
		contatoEl.appendChild(nomeEl);
		contatoEl.appendChild(telefonesEl);
		contatoEl.appendChild(emailEl);
		contatoEl.setAttribute("id", c.getId());
		
		fileHandler.addAndWrite(doc, contatoEl);
	}
	
	@Override
	public Contato buscar(String id) {
		Contato c = null;
		
		Element root = fileHandler.getRoot(doc);
		NodeList nodes = root.getElementsByTagName("contato");
		
		Element contatoEl;
		for (int i = 0; i < nodes.getLength(); i++) {
			contatoEl = (Element) nodes.item(i);
			
			if (contatoEl.getAttribute("id").equals(id)) {
				foundEl = contatoEl;
				c = convertFromEl(contatoEl);
				break;
			}
		}
		return c;
	}

	@Override
	public void remover(String id) {
		if (null == foundEl) {
			buscar(id);
		}
		fileHandler.removeAndWrite(doc, foundEl);
	}
	
	@Override
	public List<Contato> listar() {
		List<Contato> contatos = new ArrayList<Contato>();
		Element root = fileHandler.getRoot(doc);
			
		NodeList nodes = root.getElementsByTagName("contato");
		
		Element contatoEl;
		for (int i = 0; i < nodes.getLength(); i++) {
			contatoEl = (Element) nodes.item(i);
			contatos.add( convertFromEl(contatoEl) );
		}
		return contatos;
	}
	
	private Contato convertFromEl(Element el) {
		List<String> telefones = new ArrayList<String>();
		
		Element telefonesNode = (Element) el.getElementsByTagName("telefones").item(0);
		
		if (null != telefonesNode && telefonesNode.hasChildNodes()) {
			NodeList telefonesNodeList = telefonesNode.getElementsByTagName("telefone");
			
			Element telEl = null;
			for (int i = 0; i < telefonesNodeList.getLength(); i++) {
				telEl = (Element) telefonesNodeList.item(i);
				telefones.add( telEl.getTextContent() );
			}
		}
		
		return new Contato(
			telefones,
			((Element) el.getElementsByTagName("email").item(0)).getTextContent(), // >.<
			((Element) el.getElementsByTagName("nome").item(0)).getTextContent(), // >.<
			el.getAttribute("id")
		);
	}
	
	private Integer obterNovoID() {
		NodeList nl = doc.getElementsByTagName("contato");
		
		if (nl == null || nl.getLength() == 0) {
			return 1; // nao existem contatos ainda
		} else {
			Integer maiorValor = 0;
			for (int i = 0; i < nl.getLength(); i++) {
				Integer valorAtual = Integer.parseInt(nl.item(i).getAttributes().getNamedItem("id").getNodeValue());
				
				if (valorAtual > maiorValor) {
					maiorValor = valorAtual;
				}
			}
			return maiorValor + 1;
		}
	}
} //
